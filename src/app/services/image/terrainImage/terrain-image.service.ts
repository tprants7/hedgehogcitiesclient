import { Injectable } from '@angular/core';
import { ImageMatch } from 'src/app/interfaces/image-match';
import { LandSquare } from 'src/app/interfaces/land-square';
import { LandSquareService } from '../../http/square/land-square.service';
import { LineOfSquares } from 'src/app/interfaces/line-of-squares';
import { FieldOfSquares } from 'src/app/interfaces/field-of-squares';
import { SettlementImageService } from '../settlementImage/settlement-image.service';

@Injectable({
  providedIn: 'root'
})
export class TerrainImageService {
  matchList : ImageMatch[] = [
    {value : "FOREST", imgName : "forest.png"},
    {value : "PLAIN", imgName : "plain.png"},
    {value : "HILLS", imgName : "hills.png"},
    {value : "MOUNTAINS", imgName : "mountains.png"},
    {value : "SWAMP", imgName : "swamp.png"}
  ]

  constructor(
    private settlementImageService : SettlementImageService
  ) {
    //this.makeImageMatchList();
   }

  makeImageMatchList() : void {
    /*this.matchList = [
      {value : "FOREST", imgName : "LightGreen.png"},
      {value : "PLAIN", imgName : "LightOrange.png"},
      {value : "HILLS", imgName : "Lightpink.png"},
      {value : "MOUNTAINS", imgName : "50purple.png"},
      {value : "SWAMP", imgName : "50yellow.png"}
    ]*/
    /*this.matchList.push({value : "FOREST", imgName : "LightGreen.png"});
    this.matchList.push({value : "PLAIN", imgName : "LightOrange.png"});
    this.matchList.push({value : "HILLS", imgName : "Lightpink.png"});
    this.matchList.push({value : "MOUNTAINS", imgName : "50purple.png"});
    this.matchList.push({value : "SWAMP", imgName : "50yellow.png"});*/
  }

  public getMatchList() : ImageMatch[] {
    return this.matchList;
  }

  public getImageForField(field : FieldOfSquares) : FieldOfSquares {
    for(let oneLine of field.lines) {
      oneLine = this.getImageForLine(oneLine);
    }
    return field;
  }

  public getImageForLine(squareLine : LineOfSquares) : LineOfSquares {
    squareLine.squares = this.getImageForSquareArray(squareLine.squares);
    return squareLine;
  }

  public getImageForSquareArray(squares : LandSquare[]) : LandSquare[] {
    for(let oneSquare of squares) {
      oneSquare = this.getImageForSquare(oneSquare);
    }
    return squares;
  }

  public getImageForSquare(square : LandSquare) : LandSquare {
    for (let oneMatch of this.matchList) {
      this.checkAndSetImage(oneMatch, square)
    }
    if(square.settlement != null) {
      this.settlementImageService.getImageForSettlement(square.settlement);
    }
    return square;
  }

  private checkAndSetImage(imageMatch : ImageMatch, square : LandSquare) {
    if(imageMatch.value == square.terrainName) {
      square.imgName = imageMatch.imgName;
    }
  }
}
