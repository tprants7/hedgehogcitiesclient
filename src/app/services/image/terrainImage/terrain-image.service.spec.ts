import { TestBed } from '@angular/core/testing';

import { TerrainImageService } from './terrain-image.service';

describe('TerrainImageService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TerrainImageService = TestBed.get(TerrainImageService);
    expect(service).toBeTruthy();
  });
});
