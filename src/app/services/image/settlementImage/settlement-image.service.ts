import { Injectable } from '@angular/core';
import { Settlement } from 'src/app/interfaces/settlement';
import { ImageMatchNr } from 'src/app/interfaces/image-match-nr';

@Injectable({
  providedIn: 'root'
})
export class SettlementImageService {
  matchList : ImageMatchNr[] = [
    {value : 1, imgName : "village.png"},
    {value : 2, imgName : "village.png"},
    {value : 3, imgName : "city.png"},
    {value : 4, imgName : "city.png"},
  ]

  constructor() { }

  public getImageForSettlement(oneSettlement : Settlement) : Settlement {
    for(let oneMatch of this.matchList) {
      this.checkAndSetImage(oneMatch, oneSettlement);
    }
    return oneSettlement;
  }

  private checkAndSetImage(imageMatch : ImageMatchNr, oneSettlement : Settlement) {
    if(imageMatch.value == oneSettlement.lvl) {
      oneSettlement.imgName = imageMatch.imgName;
    }
  }
}
