import { TestBed } from '@angular/core/testing';

import { SettlementImageService } from './settlement-image.service';

describe('SettlementImageService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SettlementImageService = TestBed.get(SettlementImageService);
    expect(service).toBeTruthy();
  });
});
