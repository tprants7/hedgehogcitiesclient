import { TestBed } from '@angular/core/testing';

import { FieldMakerService } from './field-maker.service';

describe('FieldMakerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FieldMakerService = TestBed.get(FieldMakerService);
    expect(service).toBeTruthy();
  });
});
