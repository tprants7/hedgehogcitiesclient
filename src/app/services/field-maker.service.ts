import { Injectable } from '@angular/core';
import { MessageService } from './message.service';
import { FieldOfSquares } from '../interfaces/field-of-squares';
import { LandSquare } from '../interfaces/land-square';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Injectable({
  providedIn: 'root'
})
export class FieldMakerService {
  private field : FieldOfSquares;

  constructor(
    private messageService : MessageService
  ) { }

  /*public fieldOranizing(squares : LandSquare[]) : FieldOfSquares {
    this.field = new FieldOfSquares();
    for(var square of squares) {
      this.field.addSquare(square);
    }
    this.log("oraganized to field");
    return this.field;
  }

  private log(message: string) {
    this.messageService.add(`FieldMakerService: ${message}`);
  }*/
}
