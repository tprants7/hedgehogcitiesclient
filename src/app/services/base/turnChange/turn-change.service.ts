import { Injectable } from '@angular/core';
import * as Rx from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class TurnChangeService {
  private subject = new Rx.Subject<number>();

  constructor() { }

  public triggerNewTurn(newTurn : number) : void {
    this.subject.next(newTurn);
  }

  public getSubject() : Rx.Observable<number> {
    return this.subject;
  }
}
