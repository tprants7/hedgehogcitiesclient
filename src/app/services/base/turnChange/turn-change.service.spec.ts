import { TestBed } from '@angular/core/testing';

import { TurnChangeService } from './turn-change.service';

describe('TurnChangeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TurnChangeService = TestBed.get(TurnChangeService);
    expect(service).toBeTruthy();
  });
});
