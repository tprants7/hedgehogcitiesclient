import { TestBed } from '@angular/core/testing';

import { BaseValuesService } from './base-values.service';

describe('BaseValuesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BaseValuesService = TestBed.get(BaseValuesService);
    expect(service).toBeTruthy();
  });
});
