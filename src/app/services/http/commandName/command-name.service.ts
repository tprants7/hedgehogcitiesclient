import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseValuesService } from '../../base/baseValues/base-values.service';
import { Observable } from 'rxjs';
import { WorkNameValue } from 'src/app/interfaces/work-name-value';

@Injectable({
  providedIn: 'root'
})
export class CommandNameService {
  private url = "CommandName";

  constructor(
    private http : HttpClient,
    private baseValuesService : BaseValuesService
  ) { }

  public getCommanNames() : Observable<WorkNameValue[]> {
    const fullUrl = `${this.baseValuesService.getBaseAddress()}/${this.url}`;
    return this.http.get<WorkNameValue[]>(fullUrl);
  }
}
