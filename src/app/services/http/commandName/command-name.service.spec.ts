import { TestBed } from '@angular/core/testing';

import { CommandNameService } from './command-name.service';

describe('CommandNameService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CommandNameService = TestBed.get(CommandNameService);
    expect(service).toBeTruthy();
  });
});
