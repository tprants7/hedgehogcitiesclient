import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseValuesService } from '../../base/baseValues/base-values.service';
import { StockpileElement } from 'src/app/interfaces/stockpile-element';
import { Observable } from 'rxjs';
import { FactionStockpile } from 'src/app/interfaces/faction-stockpile';

@Injectable({
  providedIn: 'root'
})
export class StockpileService {
  private url = "stockpile";

  constructor(
    private http : HttpClient,
    private baseValueService : BaseValuesService
  ) { }

  public getFullStockpile() : Observable<StockpileElement[]> {
    const fullUrl = `${this.baseValueService.getBaseAddress()}/${this.url}`;
    return this.http.get<StockpileElement[]>(fullUrl);
  }

  public getStockpileElementById( id : number) : Observable<StockpileElement> {
    const fullUrl = `${this.baseValueService.getBaseAddress()}/${this.url}/${id}`;
    return this.http.get<StockpileElement>(fullUrl);
  }

  public getAllFactionStockpiles() : Observable<FactionStockpile[]> {
    const fullUrl = `${this.baseValueService.getBaseAddress()}/${this.url}/faction`;
    return this.http.get<FactionStockpile[]>(fullUrl);
  }
}
