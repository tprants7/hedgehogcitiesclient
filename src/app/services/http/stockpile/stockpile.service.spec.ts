import { TestBed } from '@angular/core/testing';

import { StockpileService } from './stockpile.service';

describe('StockpileService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StockpileService = TestBed.get(StockpileService);
    expect(service).toBeTruthy();
  });
});
