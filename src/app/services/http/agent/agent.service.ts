import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MessageService } from '../../message.service';
import { BaseValuesService } from '../../base/baseValues/base-values.service';
import { Observable } from 'rxjs';
import { Agent } from 'src/app/interfaces/agent';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AgentService {
  private url = "agent";

  constructor(
    private http : HttpClient,
    private baseValuesService : BaseValuesService
  ) { }

  public getAllAgents() : Observable<Agent[]> {
    const fullUrl = `${this.baseValuesService.getBaseAddress()}/${this.url}`;
    return this.http.get<Agent[]>(fullUrl);
  }

  public getOneAgent(id : number) : Observable<Agent> {
    const fullUrl = `${this.baseValuesService.getBaseAddress()}/${this.url}/${id}`;
    return this.http.get<Agent>(fullUrl);
  }

  public moveAgent(agentId : number, squareId: number) : Observable<boolean> {
    const fullUrl = `${this.baseValuesService.getBaseAddress()}/${this.url}/${agentId}/move`;
    return this.http.post<boolean>(fullUrl, squareId);
  }

  public demandTribute(agentId : number) : Observable<boolean> {
    const fullUrl = `${this.baseValuesService.getBaseAddress()}/${this.url}/${agentId}/woodTribute`;
    return this.http.post<boolean>(fullUrl, null);
  }

  public sendCommand(agentId : number, commandId : number) : Observable<boolean> {
    const fullUrl = `${this.baseValuesService.getBaseAddress()}/${this.url}/${agentId}/command`;
    return this.http.post<boolean>(fullUrl, commandId);
  }

  public startBuildProject(agentId : number, projectId : number) : Observable<boolean> {
    const fullUrl = `${this.baseValuesService.getBaseAddress()}/${this.url}/${agentId}/build`;
    return this.http.post<boolean>(fullUrl, projectId);
  }

}
