import { TestBed } from '@angular/core/testing';

import { SettlementLvlValueService } from './settlement-lvl-value.service';

describe('SettlementLvlValueService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SettlementLvlValueService = TestBed.get(SettlementLvlValueService);
    expect(service).toBeTruthy();
  });
});
