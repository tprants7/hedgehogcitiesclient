import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MessageService } from '../../message.service';
import { BaseValuesService } from '../../base/baseValues/base-values.service';
import { SettlementLvlValue } from 'src/app/interfaces/settlement-lvl-value';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SettlementLvlValueService {
  private url = "settlementLvl";

  constructor(
    private http : HttpClient,
    private messageService : MessageService,
    private baseValuesService : BaseValuesService
  ) { }

  public getLvlValue(lvl : number) : Observable<SettlementLvlValue> {
    const fullUrl = `${this.baseValuesService.getBaseAddress()}/${this.url}/${lvl}`;
    return this.http.get<SettlementLvlValue>(fullUrl);
  }

}
