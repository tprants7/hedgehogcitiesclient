import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseValuesService } from '../../base/baseValues/base-values.service';
import { Observable } from 'rxjs';
import { Faction } from 'src/app/interfaces/faction';

@Injectable({
  providedIn: 'root'
})
export class FactionService {
  private url = "factions"

  constructor(
    private http : HttpClient,
    private baseValuesService : BaseValuesService
  ) { }

  public getAllFactions() : Observable<Faction[]> {
    const fullUrl = `${this.baseValuesService.getBaseAddress()}/${this.url}`;
    return this.http.get<Faction[]>(fullUrl);
  }

  public getOneFaction(id : number) : Observable<Faction> {
    const fullUrl = `${this.baseValuesService.getBaseAddress()}/${this.url}/${id}`;
    return this.http.get<Faction>(fullUrl);
  }
}
