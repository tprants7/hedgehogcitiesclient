import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseValuesService } from '../../base/baseValues/base-values.service';
import { Observable } from 'rxjs';
import { TurnPackage } from 'src/app/interfaces/turn-package';

@Injectable({
  providedIn: 'root'
})
export class TurnCounterService {
  private url = "turn"

  constructor(
    private http : HttpClient,
    private baseValuesService : BaseValuesService
  ) { }

  public getTurnPackage() : Observable<TurnPackage> {
    const fullUrl = `${this.baseValuesService.getBaseAddress()}/${this.url}/full`;
    return this.http.get<TurnPackage>(fullUrl);
  }

  public setPause() : Observable<boolean> {
    const fullUrl = `${this.baseValuesService.getBaseAddress()}/${this.url}/pause`;
    return this.http.post<boolean>(fullUrl, true);
  }
}
