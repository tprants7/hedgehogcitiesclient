import { TestBed } from '@angular/core/testing';

import { TurnCounterService } from './turn-counter.service';

describe('TurnCounterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TurnCounterService = TestBed.get(TurnCounterService);
    expect(service).toBeTruthy();
  });
});
