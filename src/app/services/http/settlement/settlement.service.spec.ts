import { TestBed } from '@angular/core/testing';

import { SettlementService } from './settlement.service';

describe('SettlementService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SettlementService = TestBed.get(SettlementService);
    expect(service).toBeTruthy();
  });
});
