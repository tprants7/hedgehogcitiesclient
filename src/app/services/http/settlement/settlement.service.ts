import { Injectable } from '@angular/core';
import { Observable, fromEventPattern } from 'rxjs';
import { Settlement } from '../../../interfaces/settlement';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MessageService } from '../../message.service';
import { tap, map } from 'rxjs/operators';
import { BuildingProject } from 'src/app/interfaces/building-project';
import { SettlementImageService } from '../../image/settlementImage/settlement-image.service';

@Injectable({
  providedIn: 'root'
})
export class SettlementService {
  private settlementUrl = "http://localhost:8080/settlements";

  constructor(
    private http: HttpClient,
    private imageService : SettlementImageService
  ) { }

  getSettlements(): Observable<Settlement[]> {
    return this.http.get<Settlement[]>(this.settlementUrl);
  }

  getSettlement(id: number): Observable<Settlement> {
    const url = `${this.settlementUrl}/${id}`;
    return this.http.get<Settlement>(url).pipe(
      map(settlement => this.imageService.getImageForSettlement(settlement))
    );
  }

  getProjects(id : number): Observable<BuildingProject[]> {
    const url = `${this.settlementUrl}/${id}/buildingProjects`;
    return this.http.get<BuildingProject[]>(url);
  }

  getImageForSettlement(settlement : Settlement) : Settlement {
    return this.imageService.getImageForSettlement(settlement);
  }

}
