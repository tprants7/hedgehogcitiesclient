import { Injectable } from '@angular/core';
import { BaseValuesService } from '../../base/baseValues/base-values.service';
import { HttpClient } from '@angular/common/http';
import { MessageService } from '../../message.service';
import { Observable } from 'rxjs';
import { FieldOfSquares } from 'src/app/interfaces/field-of-squares';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FieldService {
  private fieldServiceURL = "/field";

  constructor(
    private baseValueService : BaseValuesService,
    private http : HttpClient,
  ) { }

  public getField(): Observable<FieldOfSquares> {
    return this.http.get<FieldOfSquares>(this.baseValueService.getBaseAddress() + this.fieldServiceURL);
  }
}
