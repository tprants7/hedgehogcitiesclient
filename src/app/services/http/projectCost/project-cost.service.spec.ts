import { TestBed } from '@angular/core/testing';

import { ProjectCostService } from './project-cost.service';

describe('ProjectCostService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProjectCostService = TestBed.get(ProjectCostService);
    expect(service).toBeTruthy();
  });
});
