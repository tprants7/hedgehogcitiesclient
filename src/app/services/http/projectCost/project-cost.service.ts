import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseValuesService } from '../../base/baseValues/base-values.service';
import { Observable } from 'rxjs';
import { StartCostSet } from 'src/app/interfaces/start-cost-set';

@Injectable({
  providedIn: 'root'
})
export class ProjectCostService {
  private url = "projectCost"

  constructor(
    private http : HttpClient,
    private baseValuesService : BaseValuesService
  ) { }

  public getAllProjectForSettlement(settlementId : number) : Observable<StartCostSet[]> {
    const fullUrl =  `${this.baseValuesService.getBaseAddress()}/${this.url}/onSettlement/${settlementId}`;
    return this.http.get<StartCostSet[]>(fullUrl);
  }

  public getProjectForDistrict(districtId : number) : Observable<StartCostSet> {
    const fullUrl = `${this.baseValuesService.getBaseAddress()}/${this.url}/forDistrict/${districtId}`;
    return this.http.get<StartCostSet>(fullUrl);
  }

  public getCanAffordForDistrict(districtId : number) : Observable<boolean> {
    const fullUrl = `${this.baseValuesService.getBaseAddress()}/${this.url}/forDistrict/${districtId}/canAfford`;
    return this.http.get<boolean>(fullUrl);
  }
}
