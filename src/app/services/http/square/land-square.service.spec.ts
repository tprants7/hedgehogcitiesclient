import { TestBed } from '@angular/core/testing';

import { LandSquareService } from './land-square.service';

describe('LandSquareService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LandSquareService = TestBed.get(LandSquareService);
    expect(service).toBeTruthy();
  });
});
