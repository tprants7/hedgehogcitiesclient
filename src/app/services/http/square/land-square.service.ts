import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MessageService } from '../../message.service';
import { Observable } from 'rxjs';
import { LandSquare } from '../../../interfaces/land-square';
import { tap, map } from 'rxjs/operators';
import { BaseValuesService } from '../../base/baseValues/base-values.service';
import { Agent } from 'src/app/interfaces/agent';
import { TerrainImageService } from '../../image/terrainImage/terrain-image.service';

@Injectable({
  providedIn: 'root'
})
export class LandSquareService {
  private landSquareUrl = "landSquares";

  constructor(
    private http : HttpClient,
    private baseValuesService : BaseValuesService,
    private terrainImageService : TerrainImageService
  ) { }

  /*public getLandSquares(): Observable<LandSquare[]> {
    const url = `${this.baseValuesService.getBaseAddress()}/${this.landSquareUrl}`;
    return this.http.get<LandSquare[]>(url);
  }*/

  public getOneLandSquare(id: number) : Observable<LandSquare> {
    const url = `${this.baseValuesService.getBaseAddress()}/${this.landSquareUrl}/${id}`;
    return this.http.get<LandSquare>(url).pipe(
      map(square => this.terrainImageService.getImageForSquare(square))
    );
  }

  public getLandSquareBySettlement(id: number) : Observable<LandSquare> {
    const url = `${this.baseValuesService.getBaseAddress()}/${this.landSquareUrl}/settlement/${id}`;
    return this.http.get<LandSquare>(url).pipe(
      map(square => this.terrainImageService.getImageForSquare(square))
    );
  }

  public getAgentsOnSquare(id: number) : Observable<Agent[]> {
    const url = `${this.baseValuesService.getBaseAddress()}/${this.landSquareUrl}/${id}/agents`;
    return this.http.get<Agent[]>(url);
  }

  /*public getOneLandSquareWithImage(id : number) : Observable<LandSquare> {
    var landSquare : LandSquare;
    this.getOneLandSquare(id).subscribe(newLandSquare => landSquare = newLandSquare);
    return this.terrainImageService.getImageForSquare(landSquare);
  }*/
}
