import { Component, OnInit } from '@angular/core';
import { LandSquare } from 'src/app/interfaces/land-square';
import { LandSquareService } from 'src/app/services/http/square/land-square.service';
import { FieldOfSquares } from 'src/app/interfaces/field-of-squares';
import { FieldMakerService } from 'src/app/services/field-maker.service';
import { MessageService } from 'src/app/services/message.service';
import { FieldService } from 'src/app/services/http/field/field.service';
import { Router } from '@angular/router';
import { TerrainImageService } from 'src/app/services/image/terrainImage/terrain-image.service';
import { ImageMatch } from 'src/app/interfaces/image-match';

@Component({
  selector: 'app-land-squares',
  templateUrl: './land-squares.component.html',
  styleUrls: ['./land-squares.component.scss']
})
export class LandSquaresComponent implements OnInit {
  //landSquares : LandSquare[];
  field : FieldOfSquares;

  constructor(
    private landSquareService : LandSquareService,
    private messageService : MessageService,
    private fieldService : FieldService,
    private router : Router,
    private imgService : TerrainImageService
  ) { }

  ngOnInit() {
    //this.getLandSquares();
    this.getField();
  }

  /*private getLandSquares() : void {
    this.landSquareService.getLandSquares().subscribe( landSquares => this.landSquares = landSquares);
  }*/

  private getField() : void {
    this.fieldService.getField().subscribe( field => this.field = this.imgService.getImageForField(field));
  }

  public navigateToDetails(id : number) : void {
    this.router.navigateByUrl(`/squares/detail/${id}`)
  }

  public receiveSquareId($event) {
    this.navigateToDetails($event);
  }



}
