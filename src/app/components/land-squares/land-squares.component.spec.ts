import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandSquaresComponent } from './land-squares.component';

describe('LandSquaresComponent', () => {
  let component: LandSquaresComponent;
  let fixture: ComponentFixture<LandSquaresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LandSquaresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandSquaresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
