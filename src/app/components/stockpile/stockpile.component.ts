import { Component, OnInit } from '@angular/core';
import { StockpileElement } from 'src/app/interfaces/stockpile-element';
import { StockpileService } from 'src/app/services/http/stockpile/stockpile.service';
import { TurnChangeService } from 'src/app/services/base/turnChange/turn-change.service';
import { Subscription } from 'rxjs';
import { FactionStockpile } from 'src/app/interfaces/faction-stockpile';

@Component({
  selector: 'app-stockpile',
  templateUrl: './stockpile.component.html',
  styleUrls: ['./stockpile.component.scss']
})
export class StockpileComponent implements OnInit {
  //public stockpile : StockpileElement[];
  private subscription: Subscription;
  public factionStockpiles : FactionStockpile[];

  constructor(
    private stockpileService : StockpileService,
    private turnChangeService : TurnChangeService
  ) { }

  ngOnInit() {
    this.getStockpile();
    this.setUpdate();
  }
  
  ngOnExit() {
    this.subscription.unsubscribe();
  }

  public getStockpile() : void {
    //this.stockpileService.getFullStockpile().subscribe(stockpile => this.stockpile = stockpile);
    this.stockpileService.getAllFactionStockpiles().subscribe(factionStockPiles => this.factionStockpiles = factionStockPiles);
  }

  private setUpdate() : void {
    this.subscription = this.turnChangeService.getSubject().subscribe(() => this.getStockpile());
  }

}
