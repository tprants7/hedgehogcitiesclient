import { Component, OnInit } from '@angular/core';
import { LandSquare } from 'src/app/interfaces/land-square';
import { ActivatedRoute } from '@angular/router';
import { LandSquareService } from 'src/app/services/http/square/land-square.service';

@Component({
  selector: 'app-square-details',
  templateUrl: './square-details.component.html',
  styleUrls: ['./square-details.component.scss']
})
export class SquareDetailsComponent implements OnInit {
  landSquare : LandSquare;

  constructor(
    private route : ActivatedRoute,
    private landSquareService : LandSquareService
  ) { }

  ngOnInit() {
    this.getSquare();
  }

  private getSquare() : void {
    const id = +this.route.snapshot.paramMap.get("id");
    this.landSquareService.getOneLandSquare(id)
      .subscribe(landSquare => this.landSquare = landSquare);
  }

}
