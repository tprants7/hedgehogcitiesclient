import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildMapComponent } from './child-map.component';

describe('ChildMapComponent', () => {
  let component: ChildMapComponent;
  let fixture: ComponentFixture<ChildMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChildMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
