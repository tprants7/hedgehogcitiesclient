import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FieldOfSquares } from 'src/app/interfaces/field-of-squares';
import { ImageMatch } from 'src/app/interfaces/image-match';
import { FieldService } from 'src/app/services/http/field/field.service';
import { TerrainImageService } from 'src/app/services/image/terrainImage/terrain-image.service';

@Component({
  selector: 'app-child-map',
  templateUrl: './child-map.component.html',
  styleUrls: ['./child-map.component.scss']
})
export class ChildMapComponent implements OnInit {
  field : FieldOfSquares;

  @Output() squareEvent = new EventEmitter<number>();

  constructor(
    private fieldService : FieldService,
    private imgService : TerrainImageService
  ) { }

  ngOnInit() {
    this.getField();
  }

  private getField() : void {
    this.fieldService.getField().subscribe(field => this.field = this.imgService.getImageForField(field));
  }
  //field => this.field = this.imgService.getImageForField(field)

  public sendSquareId(id : number) {
    this.squareEvent.emit(id);
  }

}
