import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildBuildDistrictComponent } from './child-build-district.component';

describe('ChildBuildDistrictComponent', () => {
  let component: ChildBuildDistrictComponent;
  let fixture: ComponentFixture<ChildBuildDistrictComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChildBuildDistrictComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildBuildDistrictComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
