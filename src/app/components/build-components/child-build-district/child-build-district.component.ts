import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DistrictGroup } from 'src/app/interfaces/district-group';
import { StartCostSet } from 'src/app/interfaces/start-cost-set';
import { ProjectCostService } from 'src/app/services/http/projectCost/project-cost.service';

@Component({
  selector: 'app-child-build-district',
  templateUrl: './child-build-district.component.html',
  styleUrls: ['./child-build-district.component.scss']
})
export class ChildBuildDistrictComponent implements OnInit {
  @Input() district : DistrictGroup;
  districtCost : StartCostSet;
  canAfford : boolean;

  @Output() buildEvent = new EventEmitter<number>();

  constructor(
    private projectCostService : ProjectCostService
  ) { }

  ngOnInit() {
    this.getStartCostSet();
  }

  ngOnChange() {
    this.getStartCostSet();
  }

  private getStartCostSet() : void {
    this.projectCostService.getProjectForDistrict(this.district.id)
    .subscribe(districtCost => this.districtCost = districtCost, err => {}, ()=> this.getCanAfford());
  }

  public sendStartCostSetId() {
    this.buildEvent.emit(this.districtCost.id);
  }

  public getCanAfford() {
    this.projectCostService.getCanAffordForDistrict(this.district.id)
    .subscribe(canAfford => this.canAfford = canAfford);
  }


}
