import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuildDistrictsComponent } from './build-districts.component';

describe('BuildDistrictsComponent', () => {
  let component: BuildDistrictsComponent;
  let fixture: ComponentFixture<BuildDistrictsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuildDistrictsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuildDistrictsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
