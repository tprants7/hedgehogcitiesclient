import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DistrictGroup } from 'src/app/interfaces/district-group';
import { StartCostSet } from 'src/app/interfaces/start-cost-set';

@Component({
  selector: 'app-build-districts',
  templateUrl: './build-districts.component.html',
  styleUrls: ['./build-districts.component.scss']
})
export class BuildDistrictsComponent implements OnInit {
  @Input() districts : DistrictGroup[];

  @Output() buildEvent = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {
  }

  public receiveStartCostId($event) {
    var planId = $event;
    this.sendStartCostSetId(planId);
  }

  public sendStartCostSetId(id : number) {
    this.buildEvent.emit(id);
  }

}
