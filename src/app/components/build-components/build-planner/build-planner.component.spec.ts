import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuildPlannerComponent } from './build-planner.component';

describe('BuildPlannerComponent', () => {
  let component: BuildPlannerComponent;
  let fixture: ComponentFixture<BuildPlannerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuildPlannerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuildPlannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
