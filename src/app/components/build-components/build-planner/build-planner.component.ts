import { Component, OnInit } from '@angular/core';
import { Agent } from 'src/app/interfaces/agent';
import { LandSquare } from 'src/app/interfaces/land-square';
import { ActivatedRoute } from '@angular/router';
import { AgentService } from 'src/app/services/http/agent/agent.service';
import { LandSquareService } from 'src/app/services/http/square/land-square.service';
import { ProjectCostService } from 'src/app/services/http/projectCost/project-cost.service';
import { StartCostSet } from 'src/app/interfaces/start-cost-set';
import { TerrainImageService } from 'src/app/services/image/terrainImage/terrain-image.service';

@Component({
  selector: 'app-build-planner',
  templateUrl: './build-planner.component.html',
  styleUrls: ['./build-planner.component.scss']
})
export class BuildPlannerComponent implements OnInit {
  agent : Agent;
  location : LandSquare;
  projectCosts : StartCostSet[];
  showDistrictBuilding : boolean = false;

  constructor(
    private route : ActivatedRoute,
    private agentService : AgentService,
    private squareService : LandSquareService,
    private projectCostService : ProjectCostService
  ) { }

  ngOnInit() {
    this.getAgent();
  }

  private getAgent(): void {
    const id = +this.route.snapshot.paramMap.get("id");
    this.agentService.getOneAgent(id)
    .subscribe(agent => this.agent = agent, err => {}, () => this.getLocation());
  }

  private getLocation(): void {
    this.squareService.getOneLandSquare(this.agent.location)
    .subscribe(location => this.location = location, err => {}, () => this.getProjectCosts());
  }

  private getProjectCosts(): void {
    if(this.location.settlement != null) {
      this.projectCostService.getAllProjectForSettlement(this.location.settlement.id)
      .subscribe(projectCosts => this.projectCosts = projectCosts);
    }
  }

  public receiveStartCostId($event) {
    var planId = $event;
    this.sendBuildCommand(planId);
  }

  private sendBuildCommand(projectId : number): void {
    var result : boolean;
    this.agentService.startBuildProject(this.agent.id, projectId).subscribe( newResult => console.log("Build command result was: "+newResult), err => {}, () => this.getLocation());
  }

  public flipValue(value : boolean) : boolean {
    if(value == true) {
      return false;
    }
    else {
      return true;
    }
  }

}
