import { Component, OnInit, Input } from '@angular/core';
import { Settlement } from 'src/app/interfaces/settlement';
import { SettlementLvlValue } from 'src/app/interfaces/settlement-lvl-value';
import { LandSquareService } from 'src/app/services/http/square/land-square.service';
import { SettlementLvlValueService } from 'src/app/services/http/settlementLvl/settlement-lvl-value.service';

@Component({
  selector: 'app-child-settlement-basic',
  templateUrl: './child-settlement-basic.component.html',
  styleUrls: ['./child-settlement-basic.component.scss']
})
export class ChildSettlementBasicComponent implements OnInit {
  @Input() settlement: Settlement;
  lvlValue : SettlementLvlValue;

  constructor(
    private squareService : LandSquareService,
    private lvlValueService : SettlementLvlValueService
  ) { }

  ngOnInit() {
    this.getLvlValue();
  }

  getLvlValue(): void {
    this.lvlValueService.getLvlValue(this.settlement.lvl).subscribe(lvlValue => this.lvlValue = lvlValue);
  }

}
