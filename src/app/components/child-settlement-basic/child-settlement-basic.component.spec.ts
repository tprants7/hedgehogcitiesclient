import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildSettlementBasicComponent } from './child-settlement-basic.component';

describe('ChildSettlementBasicComponent', () => {
  let component: ChildSettlementBasicComponent;
  let fixture: ComponentFixture<ChildSettlementBasicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChildSettlementBasicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildSettlementBasicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
