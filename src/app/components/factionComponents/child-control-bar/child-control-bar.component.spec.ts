import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildControlBarComponent } from './child-control-bar.component';

describe('ChildControlBarComponent', () => {
  let component: ChildControlBarComponent;
  let fixture: ComponentFixture<ChildControlBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChildControlBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildControlBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
