import { Component, OnInit, Input } from '@angular/core';
import { ControlBar } from 'src/app/interfaces/control-bar';
import { ControlValue } from 'src/app/interfaces/control-value';

@Component({
  selector: 'app-child-control-bar',
  templateUrl: './child-control-bar.component.html',
  styleUrls: ['./child-control-bar.component.scss']
})
export class ChildControlBarComponent implements OnInit {
  @Input() controlBar : ControlBar;
  showValues : boolean = false;

  constructor() { }

  ngOnInit() {
  }

  public flipValues() {
    this.showValues = !this.showValues;
  }

  public isListEmpty(list : ControlValue[]) : boolean {
    return list.length == 0;
  }

}
