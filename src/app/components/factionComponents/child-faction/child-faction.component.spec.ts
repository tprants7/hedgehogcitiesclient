import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildFactionComponent } from './child-faction.component';

describe('ChildFactionComponent', () => {
  let component: ChildFactionComponent;
  let fixture: ComponentFixture<ChildFactionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChildFactionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildFactionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
