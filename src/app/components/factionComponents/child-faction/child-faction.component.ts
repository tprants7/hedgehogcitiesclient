import { Component, OnInit, Input } from '@angular/core';
import { Faction } from 'src/app/interfaces/faction';

@Component({
  selector: 'app-child-faction',
  templateUrl: './child-faction.component.html',
  styleUrls: ['./child-faction.component.scss']
})
export class ChildFactionComponent implements OnInit {
  @Input() faction : Faction;

  constructor() { }

  ngOnInit() {
  }

}
