import { Component, OnInit } from '@angular/core';
import { Faction } from 'src/app/interfaces/faction';
import { FactionService } from 'src/app/services/http/faction/faction.service';

@Component({
  selector: 'app-factions',
  templateUrl: './factions.component.html',
  styleUrls: ['./factions.component.scss']
})
export class FactionsComponent implements OnInit {
  factions : Faction[];

  constructor(
    private factionService : FactionService
  ) { }

  ngOnInit() {
    this.getFactions();
  }

  private getFactions() : void {
    this.factionService.getAllFactions().subscribe(factions => this.factions = factions);
  }

}
