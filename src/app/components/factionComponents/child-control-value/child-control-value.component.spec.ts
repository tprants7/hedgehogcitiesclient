import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildControlValueComponent } from './child-control-value.component';

describe('ChildControlValueComponent', () => {
  let component: ChildControlValueComponent;
  let fixture: ComponentFixture<ChildControlValueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChildControlValueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildControlValueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
