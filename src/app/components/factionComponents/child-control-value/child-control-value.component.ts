import { Component, OnInit, Input } from '@angular/core';
import { ControlValue } from 'src/app/interfaces/control-value';

@Component({
  selector: 'app-child-control-value',
  templateUrl: './child-control-value.component.html',
  styleUrls: ['./child-control-value.component.scss']
})
export class ChildControlValueComponent implements OnInit {
  @Input() controlValue : ControlValue;

  constructor() { }

  ngOnInit() {
  }

}
