import { Component, OnInit } from '@angular/core';
import { Faction } from 'src/app/interfaces/faction';
import { ActivatedRoute } from '@angular/router';
import { FactionService } from 'src/app/services/http/faction/faction.service';
import { Settlement } from 'src/app/interfaces/settlement';
import { SettlementService } from 'src/app/services/http/settlement/settlement.service';

@Component({
  selector: 'app-faction-details',
  templateUrl: './faction-details.component.html',
  styleUrls: ['./faction-details.component.scss']
})
export class FactionDetailsComponent implements OnInit {
  faction : Faction;
  capital : Settlement;

  constructor(
    private route : ActivatedRoute,
    private factionService : FactionService,
    private settlementService : SettlementService
  ) { }

  ngOnInit() {
    this.getFaction();
  }

  private getFaction(): void {
    const id = +this.route.snapshot.paramMap.get("id");
    this.factionService.getOneFaction(id)
    .subscribe(faction => this.faction = faction, err => {}, () => this.getCapital());
  }

  private getCapital() : void {
    if(this.faction.capitalId) {
      this.settlementService.getSettlement(this.faction.capitalId)
      .subscribe(capital => this.capital = capital);
    }
  }

}
