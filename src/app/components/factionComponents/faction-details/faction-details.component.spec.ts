import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FactionDetailsComponent } from './faction-details.component';

describe('FactionDetailsComponent', () => {
  let component: FactionDetailsComponent;
  let fixture: ComponentFixture<FactionDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FactionDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FactionDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
