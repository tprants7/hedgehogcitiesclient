import { Component, OnInit, Input } from '@angular/core';
import { FactionStockpile } from 'src/app/interfaces/faction-stockpile';

@Component({
  selector: 'app-child-faction-stockpile',
  templateUrl: './child-faction-stockpile.component.html',
  styleUrls: ['./child-faction-stockpile.component.scss']
})
export class ChildFactionStockpileComponent implements OnInit {
  @Input() factionStockpile : FactionStockpile;

  constructor() { }

  ngOnInit() {
  }

}
