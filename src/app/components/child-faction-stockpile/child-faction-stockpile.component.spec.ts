import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildFactionStockpileComponent } from './child-faction-stockpile.component';

describe('ChildFactionStockpileComponent', () => {
  let component: ChildFactionStockpileComponent;
  let fixture: ComponentFixture<ChildFactionStockpileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChildFactionStockpileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildFactionStockpileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
