import { Component, OnInit } from '@angular/core';
import { TurnPackage } from 'src/app/interfaces/turn-package';
import { TurnCounterService } from 'src/app/services/http/turnCounter/turn-counter.service';
import { Observable, timer, Subscription } from 'rxjs';
import { TurnChangeService } from 'src/app/services/base/turnChange/turn-change.service';

@Component({
  selector: 'app-turn-counter',
  templateUrl: './turn-counter.component.html',
  styleUrls: ['./turn-counter.component.scss']
})
export class TurnCounterComponent implements OnInit {
  private subscription: Subscription;
  turnPackage : TurnPackage;
  //turn : number;
  timeTill : number;
  everySecond: Observable<number> = timer(0, 1000);

  constructor(
    private turnCounterService : TurnCounterService,
    private turnChangeService : TurnChangeService
  ) { }

  ngOnInit() {
    this.subscription = this.everySecond.subscribe((seconds) => {
      if(this.turnPackage == null) {
        this.getTurnPackage();
        console.log("timer was null");
      }
      else {
        //if(this.turnPackage.pause == false) {
          if(this.turnPackage.timeTill < 1) {
            this.getTurnPackage();
            console.log("Got new time package");
          }
          else {
            if(this.turnPackage.paused == false){
              this.turnPackage.timeTill--;
            }
            
            //console.log("counted down");
          }
        //}
      }
    }
    )
  }

  getTurnPackage(): void {
    this.turnCounterService.getTurnPackage()
      .subscribe(turnPackage => this.turnPackage = turnPackage, err => {}, () => this.sendNewTurnSignal(this.turnPackage.turn));
  }

  togglePause(): void {
    this.turnCounterService.setPause().subscribe(() => null, err => {}, () => this.getTurnPackage());
  }

  sendNewTurnSignal(newTurn : number) : void {
    this.turnChangeService.triggerNewTurn(newTurn);
  }


}
