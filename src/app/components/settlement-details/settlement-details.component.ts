import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SettlementService } from 'src/app/services/http/settlement/settlement.service';
import { Settlement } from 'src/app/interfaces/settlement';
import { LandSquareService } from 'src/app/services/http/square/land-square.service';
import { LandSquare } from 'src/app/interfaces/land-square';
import { SettlementLvlValue } from 'src/app/interfaces/settlement-lvl-value';
import { SettlementLvlValueService } from 'src/app/services/http/settlementLvl/settlement-lvl-value.service';
import { TurnChangeService } from 'src/app/services/base/turnChange/turn-change.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-settlement-details',
  templateUrl: './settlement-details.component.html',
  styleUrls: ['./settlement-details.component.scss']
})
export class SettlementDetailsComponent implements OnInit {
  settlement: Settlement;
  private subscription: Subscription;

  constructor(
    private route: ActivatedRoute,
    private settlementService: SettlementService,
    private turnChangeService : TurnChangeService,
  ) { }

  ngOnInit() {
    this.getSettlement();
    this.setUpdate();
  }

  ngOnExit() {
    this.subscription.unsubscribe();
  }

  getSettlement(): void {
    const id = +this.route.snapshot.paramMap.get("id");
    this.settlementService.getSettlement(id)
      .subscribe(settlement => this.settlement = settlement);
  }

  private setUpdate() : void {
    this.subscription = this.turnChangeService.getSubject().subscribe(() => this.getSettlement());
  }
}
