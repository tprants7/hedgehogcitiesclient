import { Component, OnInit, Input } from '@angular/core';
import { LandSquare } from 'src/app/interfaces/land-square';
import { TerrainImageService } from 'src/app/services/image/terrainImage/terrain-image.service';

@Component({
  selector: 'app-child-square',
  templateUrl: './child-square.component.html',
  styleUrls: ['./child-square.component.scss']
})
export class ChildSquareComponent implements OnInit {
  @Input() square : LandSquare;

  constructor(
    private imageService : TerrainImageService
  ) { }

  ngOnInit() {
    this.square = this.checkForImage(this.square);
  }

  checkForImage(oneSquare : LandSquare) : LandSquare {
    if(oneSquare.imgName == null) {
      return this.getImageForSquare(oneSquare);
    }
    else {
      return oneSquare;
    }
  }

  getImageForSquare(oneSquare : LandSquare) : LandSquare {
    return this.imageService.getImageForSquare(oneSquare);
  }

}
