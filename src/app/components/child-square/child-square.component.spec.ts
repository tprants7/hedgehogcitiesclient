import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildSquareComponent } from './child-square.component';

describe('ChildSquareComponent', () => {
  let component: ChildSquareComponent;
  let fixture: ComponentFixture<ChildSquareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChildSquareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildSquareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
