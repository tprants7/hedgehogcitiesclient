import { Component, OnInit } from '@angular/core';
import { Agent } from 'src/app/interfaces/agent';
import { ActivatedRoute } from '@angular/router';
import { AgentService } from 'src/app/services/http/agent/agent.service';
import { LandSquare } from 'src/app/interfaces/land-square';
import { LandSquareService } from 'src/app/services/http/square/land-square.service';
import { WorkNameValue } from 'src/app/interfaces/work-name-value';
import { CommandNameService } from 'src/app/services/http/commandName/command-name.service';
import { StartCostSet } from 'src/app/interfaces/start-cost-set';
import { ProjectCostService } from 'src/app/services/http/projectCost/project-cost.service';

@Component({
  selector: 'app-agent-details',
  templateUrl: './agent-details.component.html',
  styleUrls: ['./agent-details.component.scss']
})
export class AgentDetailsComponent implements OnInit {
  agent : Agent;
  location : LandSquare;
  showMap : boolean = false;
  clickSquareId : number;
  possibleWorks : WorkNameValue[];
  respondBool : boolean;

  constructor(
    private route : ActivatedRoute,
    private agentService : AgentService,
    private squareService : LandSquareService,
    private commandNameService : CommandNameService,
  ) { }

  ngOnInit() {
    this.getAgent();
  }

  private getAgent(): void {
    const id = +this.route.snapshot.paramMap.get("id");
    this.agentService.getOneAgent(id)
    .subscribe(agent => this.agent = agent, err => {}, () => this.getLocation());
  }

  private getLocation(): void {
    this.squareService.getOneLandSquare(this.agent.location)
    .subscribe(location => this.location = location, err => {}, () => this.getCommands());
  }

  private getCommands(): void {
    this.commandNameService.getCommanNames()
    .subscribe(commandNames => this.possibleWorks = commandNames)
  }

  public flipMap(): void {
    if(this.showMap == false) {
      this.showMap = true;
    }
    else {
      this.showMap = false;
    }
  }

  public receiveSquareId($event) {
    this.clickSquareId = $event;
    this.moveAgent(this.clickSquareId);
  }

  public moveAgent(squareId : number): void {
    this.agentService.moveAgent(this.agent.id, squareId).subscribe(response => this.respondBool = response, err => {}, () => this.getAgent());
  }

  /*public doTributeCommand() : void {
    this.agentService.demandTribute(this.agent.id).subscribe(response => this.tributeResponse = response);
  }*/

  public doAgentCommand(commandNumber : number) {
    this.agentService.sendCommand(this.agent.id, commandNumber).subscribe(response => {}, err => {}, () => this.getAgent());
  }


}
