import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildSettlementComponent } from './child-settlement.component';

describe('ChildSettlementComponent', () => {
  let component: ChildSettlementComponent;
  let fixture: ComponentFixture<ChildSettlementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChildSettlementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildSettlementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
