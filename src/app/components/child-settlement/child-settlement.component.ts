import { Component, OnInit, Input } from '@angular/core';
import { Settlement } from 'src/app/interfaces/settlement';
import { LandSquare } from 'src/app/interfaces/land-square';
import { SettlementLvlValue } from 'src/app/interfaces/settlement-lvl-value';
import { LandSquareService } from 'src/app/services/http/square/land-square.service';
import { SettlementLvlValueService } from 'src/app/services/http/settlementLvl/settlement-lvl-value.service';
import { Agent } from 'src/app/interfaces/agent';
import { BuildingProject } from 'src/app/interfaces/building-project';
import { SettlementService } from 'src/app/services/http/settlement/settlement.service';

@Component({
  selector: 'app-child-settlement',
  templateUrl: './child-settlement.component.html',
  styleUrls: ['./child-settlement.component.scss']
})
export class ChildSettlementComponent implements OnInit {
  @Input() settlement: Settlement;
  square : LandSquare;
  lvlValue : SettlementLvlValue;
  showDist : boolean;
  showLoc : boolean;
  agents : Agent[];
  buildProjects : BuildingProject[];
  showAgentLink : boolean = true;
  showProjectList : boolean = false;

  constructor(
    private squareService : LandSquareService,
    private lvlValueService : SettlementLvlValueService,
    private settlementService : SettlementService
  ) { }

  ngOnInit() {
    this.getLvlValue();
    this.getSquare();
    this.getBuildingProjects();
    this.showDist = false;
    this.showLoc = false;
  }

  getLvlValue(): void {
    this.lvlValueService.getLvlValue(this.settlement.lvl).subscribe(lvlValue => this.lvlValue = lvlValue);
  }

  getSquare(): void {
    const id = this.settlement.id;
    this.squareService.getLandSquareBySettlement(id)
      .subscribe(square => this.square = square, err => {}, () => this.getAgentsOnSquare());
  }

  getAgentsOnSquare(): void {
    const id = this.square.id
    this.squareService.getAgentsOnSquare(id)
      .subscribe(agents => this.agents = agents);
  }

  getBuildingProjects(): void {
    this.settlementService.getProjects(this.settlement.id).subscribe(projects => this.buildProjects = projects);
  }

  flipDist() : void {
    this.showDist = !this.showDist;
  }

  flipLoc() : void {
    this.showLoc = !this.showLoc;
  }

  flipProg() : void {
    this.showProjectList = !this.showProjectList;
  }

}
