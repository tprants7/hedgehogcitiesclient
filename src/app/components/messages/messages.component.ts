import { Component, OnInit, Input } from '@angular/core';
import { MessageService } from 'src/app/services/message.service';
import { Settlement } from 'src/app/interfaces/settlement';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss']
})
export class MessagesComponent implements OnInit {
  @Input() settlement: Settlement;

  constructor(public messageService: MessageService) { }

  ngOnInit() {
  }

}
