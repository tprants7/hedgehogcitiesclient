import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildDistrictComponent } from './child-district.component';

describe('ChildDistrictComponent', () => {
  let component: ChildDistrictComponent;
  let fixture: ComponentFixture<ChildDistrictComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChildDistrictComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildDistrictComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
