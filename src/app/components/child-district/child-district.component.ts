import { Component, OnInit, Input } from '@angular/core';
import { DistrictGroup } from 'src/app/interfaces/district-group';

@Component({
  selector: 'app-child-district',
  templateUrl: './child-district.component.html',
  styleUrls: ['./child-district.component.scss']
})
export class ChildDistrictComponent implements OnInit {

  @Input() district : DistrictGroup;

  constructor() { }

  ngOnInit() {
  }

}
