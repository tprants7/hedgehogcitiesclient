import { Component, OnInit } from '@angular/core';
import { Agent } from 'src/app/interfaces/agent';
import { AgentService } from 'src/app/services/http/agent/agent.service';

@Component({
  selector: 'app-agents',
  templateUrl: './agents.component.html',
  styleUrls: ['./agents.component.scss']
})
export class AgentsComponent implements OnInit {

  agents : Agent[];

  constructor(
    private agentService : AgentService
  ) { }

  ngOnInit() {
    this.getAgents();
  }

  private getAgents() : void {
    this.agentService.getAllAgents().subscribe(agents => this.agents = agents);
  }

}
