import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildStockpileElementComponent } from './child-stockpile-element.component';

describe('ChildStockpileElementComponent', () => {
  let component: ChildStockpileElementComponent;
  let fixture: ComponentFixture<ChildStockpileElementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChildStockpileElementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildStockpileElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
