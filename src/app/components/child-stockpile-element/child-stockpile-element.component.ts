import { Component, OnInit, Input } from '@angular/core';
import { StockpileElement } from 'src/app/interfaces/stockpile-element';

@Component({
  selector: 'app-child-stockpile-element',
  templateUrl: './child-stockpile-element.component.html',
  styleUrls: ['./child-stockpile-element.component.scss']
})
export class ChildStockpileElementComponent implements OnInit {

  @Input() sElement : StockpileElement;

  constructor() { }

  ngOnInit() {
  }

}
