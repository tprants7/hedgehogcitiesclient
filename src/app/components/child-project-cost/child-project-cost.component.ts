import { Component, OnInit, Input } from '@angular/core';
import { StartCostSet } from 'src/app/interfaces/start-cost-set';

@Component({
  selector: 'app-child-project-cost',
  templateUrl: './child-project-cost.component.html',
  styleUrls: ['./child-project-cost.component.scss']
})
export class ChildProjectCostComponent implements OnInit {
  @Input() projectCost : StartCostSet;

  constructor() { }

  ngOnInit() {
  }

}
