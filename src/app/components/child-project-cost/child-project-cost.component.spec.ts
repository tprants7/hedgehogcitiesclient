import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildProjectCostComponent } from './child-project-cost.component';

describe('ChildProjectCostComponent', () => {
  let component: ChildProjectCostComponent;
  let fixture: ComponentFixture<ChildProjectCostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChildProjectCostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildProjectCostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
