import { Component, OnInit } from '@angular/core';
import { Settlement } from 'src/app/interfaces/settlement';
import { SettlementService } from 'src/app/services/http/settlement/settlement.service';
import { LandSquareService } from 'src/app/services/http/square/land-square.service';

@Component({
  selector: 'app-settlements',
  templateUrl: './settlements.component.html',
  styleUrls: ['./settlements.component.scss']
})
export class SettlementsComponent implements OnInit {

  settlements : Settlement[];

  constructor(
    private settlementService: SettlementService
  ) { }

  ngOnInit() {
    this.getSettlements();
  }

  getSettlements(): void {
    this.settlementService.getSettlements()
      .subscribe(settlements => this.settlements = settlements);
  }

}
