import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildAgentComponent } from './child-agent.component';

describe('ChildAgentComponent', () => {
  let component: ChildAgentComponent;
  let fixture: ComponentFixture<ChildAgentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChildAgentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildAgentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
