import { Component, OnInit, Input } from '@angular/core';
import { LandSquare } from 'src/app/interfaces/land-square';
import { LandSquareService } from 'src/app/services/http/square/land-square.service';
import { Agent } from 'src/app/interfaces/agent';

@Component({
  selector: 'app-child-agent',
  templateUrl: './child-agent.component.html',
  styleUrls: ['./child-agent.component.scss']
})
export class ChildAgentComponent implements OnInit {
  @Input() agent : Agent;
  @Input() showDetailLink : boolean = false;

  constructor() { }

  ngOnInit() {
  }

}
