export interface StockpileElement {
    id : number;
    materialName : String;
    amount : number;
}
