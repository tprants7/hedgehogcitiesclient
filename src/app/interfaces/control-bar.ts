import { ControlValue } from './control-value';

export interface ControlBar {
    id : number;
    maxDynamicPoints : number;
    controlValues : ControlValue[];
    maxAllPoints : number;
    allClaimedPoints : number;
    allStaticPoints : number;
}
