import { StockpileElement } from './stockpile-element';

export interface StartCostSet {
    id : number;
    elementCosts : StockpileElement[];
    target : String;
    projectSize : number;
}
