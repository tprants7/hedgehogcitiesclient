import { Settlement } from './settlement';

export interface LandSquare {
    id : number;
    terrainName : String;
    settlement : Settlement;
    xcoord : number;
    ycoord : number;
    imgName : String;
}
