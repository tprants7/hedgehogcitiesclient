import { DistrictGroup } from './district-group';
import { ControlBar } from './control-bar';

export interface Settlement {
    id : number;
    name : string;
    lvl : number;
    districts : DistrictGroup[];
    wealthCeiling : number;
    wealth : number;
    wealthGrowth : number;
    imgName : string;
    controlDetails : ControlBar;

}
