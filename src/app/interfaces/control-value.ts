import { Faction } from './faction';

export interface ControlValue {
    id : number;
    faction : Faction;
    controlPoints : number;
    controlType : String;
}
