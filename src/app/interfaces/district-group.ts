export interface DistrictGroup {
    id : number;
    type : string;
    currentAmount : number;
    maxAmount : number;
}
