import { LandSquare } from './land-square';

export interface Agent {
    id : number;
    name : String;
    role : String;
    location : number;
    currentWork : String;
}
