import { LineOfSquares } from './line-of-squares';
import { LandSquaresComponent } from '../components/land-squares/land-squares.component';
import { LandSquare } from './land-square';

export interface FieldOfSquares {
    name : String;
    lines : LineOfSquares[];
}
