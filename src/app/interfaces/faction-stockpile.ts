import { Faction } from './faction';
import { StockpileElement } from './stockpile-element';

export interface FactionStockpile {
    id : number;
    faction : Faction;
    elements : StockpileElement[];
}
