export interface TurnPackage {
    turn : number;
    timeTill : number;
    paused : boolean;
}
