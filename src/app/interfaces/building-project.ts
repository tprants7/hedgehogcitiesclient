export interface BuildingProject {
    id : number;
    location : number;
    buildTarget : String;
    progress : number;
    finishMark : number;
}
