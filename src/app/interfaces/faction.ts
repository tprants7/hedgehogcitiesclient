export interface Faction {
    id : number;
    name : String;
    capitalId : number;
}
