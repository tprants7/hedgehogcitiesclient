import { LandSquare } from './land-square';

export interface LineOfSquares {
    squares : LandSquare[];
    ycoord : String;
}
