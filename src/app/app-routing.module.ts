import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SettlementsComponent } from './components/settlements/settlements.component';
import { SettlementDetailsComponent } from './components/settlement-details/settlement-details.component';
import { LandSquaresComponent } from './components/land-squares/land-squares.component';
import { SquareDetailsComponent } from './components/square-details/square-details.component';
import { AgentsComponent } from './components/agents/agents.component';
import { AgentDetailsComponent } from './components/agent-details/agent-details.component';
import { StockpileComponent } from './components/stockpile/stockpile.component';
import { BuildPlannerComponent } from './components/build-components/build-planner/build-planner.component';
import { FactionsComponent } from './components/factionComponents/factions/factions.component';
import { FactionDetailsComponent } from './components/factionComponents/faction-details/faction-details.component';

const routes: Routes = [
  { path: "settlements", component: SettlementsComponent },
  { path: "settlements/detail/:id", component: SettlementDetailsComponent },
  { path: "squares", component: LandSquaresComponent },
  { path: "squares/detail/:id", component: SquareDetailsComponent},
  { path: "agents", component: AgentsComponent},
  { path: "agents/detail/:id", component: AgentDetailsComponent},
  { path: "agents/build/:id", component: BuildPlannerComponent},
  { path: "stockpile", component: StockpileComponent },
  { path: "factions", component: FactionsComponent },
  { path: "factions/detail/:id", component: FactionDetailsComponent },
  { path: "", redirectTo: "/settlements", pathMatch: "full"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
