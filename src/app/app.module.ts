import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SettlementsComponent } from './components/settlements/settlements.component';
import { HttpClientModule } from '@angular/common/http';
import { MessagesComponent } from './components/messages/messages.component';
import { SettlementDetailsComponent } from './components/settlement-details/settlement-details.component';
import { LandSquaresComponent } from './components/land-squares/land-squares.component';
import { SquareDetailsComponent } from './components/square-details/square-details.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { AgentsComponent } from './components/agents/agents.component';
import { AgentDetailsComponent } from './components/agent-details/agent-details.component';
import { ChildMapComponent } from './components/child-map/child-map.component';
import { StockpileComponent } from './components/stockpile/stockpile.component';
import { TurnCounterComponent } from './components/turn-counter/turn-counter.component';
import { ChildDistrictComponent } from './components/child-district/child-district.component';
import { ChildSquareComponent } from './components/child-square/child-square.component';
import { TopBarComponent } from './components/top-bar/top-bar.component';
import { ChildStockpileElementComponent } from './components/child-stockpile-element/child-stockpile-element.component';
import { ChildSettlementComponent } from './components/child-settlement/child-settlement.component';
import { ChildAgentComponent } from './components/child-agent/child-agent.component';
import { ChildProjectCostComponent } from './components/child-project-cost/child-project-cost.component';
import { BuildPlannerComponent } from './components/build-components/build-planner/build-planner.component';
import { ChildSettlementBasicComponent } from './components/child-settlement-basic/child-settlement-basic.component';
import { BuildDistrictsComponent } from './components/build-components/build-districts/build-districts.component';
import { ChildBuildDistrictComponent } from './components/build-components/child-build-district/child-build-district.component';
import { ChildControlBarComponent } from './components/factionComponents/child-control-bar/child-control-bar.component';
import { ChildFactionComponent } from './components/factionComponents/child-faction/child-faction.component';
import { ChildControlValueComponent } from './components/factionComponents/child-control-value/child-control-value.component';
import { FactionsComponent } from './components/factionComponents/factions/factions.component';
import { FactionDetailsComponent } from './components/factionComponents/faction-details/faction-details.component';
import { ChildFactionStockpileComponent } from './components/child-faction-stockpile/child-faction-stockpile.component';

@NgModule({
  declarations: [
    AppComponent,
    SettlementsComponent,
    MessagesComponent,
    SettlementDetailsComponent,
    LandSquaresComponent,
    SquareDetailsComponent,
    NavigationComponent,
    AgentsComponent,
    AgentDetailsComponent,
    ChildMapComponent,
    StockpileComponent,
    TurnCounterComponent,
    ChildDistrictComponent,
    ChildSquareComponent,
    TopBarComponent,
    ChildStockpileElementComponent,
    ChildSettlementComponent,
    ChildAgentComponent,
    ChildProjectCostComponent,
    BuildPlannerComponent,
    ChildSettlementBasicComponent,
    BuildDistrictsComponent,
    ChildBuildDistrictComponent,
    ChildControlBarComponent,
    ChildFactionComponent,
    ChildControlValueComponent,
    FactionsComponent,
    FactionDetailsComponent,
    ChildFactionStockpileComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
